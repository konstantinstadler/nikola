# Source: https://gitlab.com/paddy-hack/nikola under GPL 3+
# 1. Modified to fix issues the source image had when 
# 	 tried to add ipynb support via "pip install jupyter".
# 2. Added built-in support for ipynb files.
# 3. Upgraded OS to alpine 3.12
# 4. Upgraded to Nikola's latest build 8.1.0
# 
# This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
# (CC BY-SA 4.0)
# https://creativecommons.org/licenses/by-sa/4.0/

FROM        alpine:3.12
MAINTAINER  xplusy <gitlab.xplusy@krishna.eu.org>

ENV PIP_OPTS --no-cache-dir --disable-pip-version-check
ARG _VERSION

RUN apk update
RUN apk add --no-cache                                                  \
        python3                                                         \
        libxml2                                                         \
        libxslt                                                         \
        jpeg                                                            \
        zeromq-dev                                                      \
        libstdc++                                                       \
        py3-six                                                         \
        py3-requests                                                    \
        openssh                                                         \
        sshpass                                                         \
                                                                     && \
    apk add --no-cache --virtual .build-deps                            \
        gcc                                                             \
        g++                                                             \
        linux-headers                                                   \
        musl-dev                                                        \
        python3-dev                                                     \
        py3-pip                                                         \
        libxml2-dev                                                     \
        libxslt-dev                                                     \
        jpeg-dev                                                        \
                                                                     && \
    CFLAGS="$CFLAGS -L/lib"                                             \
    pip3 install $PIP_OPTS setuptools jupyter 'nikola[extras]'$_VERSION \
                                                                     && \
    apk del .build-deps                                              && \
    find /usr/lib/python3.*                                             \
        \( -type d -a -name test -o -name tests \)                      \
        -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \)              \
        -exec rm -rf '{}' +

