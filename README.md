# Nikola image for GitLab pages
[![badge](https://img.shields.io/badge/link-to%20repository-orange.svg)](https://gitlab.com/registryimage/nikola)
[![badge](https://badges.gitter.im/nikola-ssg/community.svg)](https://gitter.im/nikola-ssg/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

## History
Based on [work](https://gitlab.com/paddy-hack/nikola/) by Olaf Meeuwissen.

I created this image as the Nikola image above had dependency issues when attempted to run in Jupyter notebooks (ipynb files). The instructions given at the source link did not work. I raised an issue but having had no response when I wrote this, I decided to give a try to create a new image. As a newbie to GitLab and CI/CD in general, and docker, YAML, etc, this was quite a learning experience for me. 

I started building with different base images and found out that alpine had the smallest image size and had the fastest docker image load times. Following are the approximate sizes and GitLab pages build times of operating systems/base images I tried.

| Base image | Image size | GitLab pages build time`*` |
|:- |:- |:- |
| Windows server | >500MB | > 1 minute |
| Ubuntu Linux | ~300MB | ~ 50 seconds |
| Python official image | ~250MB | ~ 50 seconds |
| Alpine Linux | ~55MB | < 30 seconds |  

`*` Test site was very basic with 3 simple posts and an image. GitLab pages build time includes docker image load time + nikola build script + pages deployment.



## Updates
![badge](https://img.shields.io/badge/nikola-8.1.1-blue.svg)
![badge](https://img.shields.io/badge/alpine-3.12-blue.svg)
![badge](https://img.shields.io/badge/image%20size-58.98%20MB-lightgrey.svg)
![badge](https://img.shields.io/badge/build-passing-green.svg)
![badge](https://img.shields.io/badge/dependencies-up%20to%20date-green.svg)
[![badge](https://img.shields.io/badge/licence-CC%20BY--SA%204.0-9cf.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

**2020-07-09:**  

1. Updated to Nikola 8.1.1

**2020-07-03:**  

Thanks to the original code at the source. I only made modifications to accommodate the following:

1. Fixed dependency issues in the original Nikola image that would throw errors while processing ipynb files even after adding "pip3 install jupyter". Ref: [original nikola image issue](https://gitlab.com/paddy-hack/nikola/-/issues/10)
2. Added built-in support to compile `ipynb` files (Jupyter notebooks).
3. Upgraded base docker image from alpine 3.9 to alpine 3.12 and added required dependencies as 3.12 was slightly more bare than 3.9.
4. Upgrated to the latest version of Nikola 8.1.0. In future, I'll try to upgrade to the latest version of Nikola whenever it comes out, but if I don't please raise an issue and I'll do it. Or you can fork this repo and make your own image like I did.

## Usage
Add the following to your .gitlab-ci.yml file:
```
image: registry.gitlab.com/registryimage/nikola

pages:
  script:
    - nikola build
    - mv output public
  artifacts:
    paths:
    - public
  only:
  - master

```

## Optional
1. In case, you use a different output folder in your Nikola conf.py file, then change the line `- mv output public` to the following:
`- mv <your-output-directory> public`
2. If your Nikole site output directory is configured to "public" then remove the line `- mv output public`.

## Finally
Enjoy using [Nikola](https://getnikola.com) with GitLab pages.



<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
